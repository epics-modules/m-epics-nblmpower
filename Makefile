# include $(EPICS_ENV_PATH)/module.Makefile

#STARTUPS = startup/nblmpower.cmd
# DOC = doc/README.md
# MISCS += misc/one_shot_button.py

#USR_DEPENDENCIES = <module>,<version>

PROJECT=nblmpower

include ${EPICS_ENV_PATH}/module.Makefile

# We need an explicit dependency on nds3epics.
# USR_DEPENDENCIES = ifcdaqdrv2,1.0.0

USR_CXXFLAGS = -std=c++0x

OPIS = opi

