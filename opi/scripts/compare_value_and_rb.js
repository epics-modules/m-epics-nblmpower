importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

ROUND_PREC = 3  // round() precision

var value 		= Math.round(PVUtil.getDouble(pvs[0]), ROUND_PREC);	// value: setter (IOC). This value is send to device (CAEN crate, FPGA, ...).
var value_rb 	= Math.round(PVUtil.getDouble(pvs[1]), ROUND_PREC); 	// read-back value of the setter (the actual value on the CAEN crate / FPGA)

// debug
// ConsoleUtil.writeInfo("value:" + value);
// ConsoleUtil.writeInfo("RB value:" + value_rb);

if(value != value_rb){
	widget.setPropertyValue("border_style",1); // line style
	widget.setPropertyValue("border_color","Major");
	// ConsoleUtil.writeInfo("error RB");
}
else {
widget.setPropertyValue("border_style",3); // lowered style
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,255,255));
	// ConsoleUtil.writeInfo("ok");
}

