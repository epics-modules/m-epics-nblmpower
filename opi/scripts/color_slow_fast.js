importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 
var pv0 = PVUtil.getString(pvs[0]);

if(pv0=="SLOW")
	// green
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,128,0));
else if(pv0=="FAST")
	// red
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(255,0,0));
else {
	// blue = error
	widget.setPropertyValue("border_color",ColorFontUtil.getColorFromRGB(0,148,202));
	ConsoleUtil.writeInfo("color slow fast error: "+ pv0);
}

// ConsoleUtil.writeInfo("pv0: "+ pv0);
